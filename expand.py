import sys
def expand(s):
        r = []
        for i in s.split(','):
                if '-' not in i:
                        r.append(int(i))
                else:
                        l,h = map(int, i.split('-'))
                        r+= range(l,h+1)
        return ','.join(str(x) for x in r)

print expand(sys.argv[1])
